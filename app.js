// declare arrays and prices of waiting for car types
const ARRAY_UBER_X_PRICE = [8000, 12000, 10000];
const UBER_X_WAIT_PRICE= 2000;

const ARRAY_UBER_SUV_PRICE = [9000, 14000, 12000];
const UBER_SUV_WAIT_PRICE = 3000;

const ARRAY_UBER_BLACK_PRICE = [10000, 16000, 14000];
const UBER_BLACK_WAIT_PRICE = 4000;

//The checked property sets or returns the checked state of a radio button.
//This property reflects the HTML checked attribute.
//syntax: radioObject.checked

function checkCarType() {
    var uberX = document.getElementById("uberX");
    var uberSUV = document.getElementById("uberSUV");
    var uberBlack = document.getElementById("uberBlack");
    if (uberX.checked) {
        return "uberX";
    }
    else if (uberSUV.checked) {
        return "uberSUV";
    }
    else if (uberBlack.checked) {
        return "uberBlack";
    }
}
//waitingTime>= 3 minutes , each 3 mins, calculate 1 time-> caculate waitingPrice
function priceOfWait(waitingTime, waitingPrice) {
    var waitingPay = 0;
    if (waitingTime >= 3) {
        waitingPay = waitingPrice * Math.round(waitingTime / 3.0);
    }
    return waitingPay;
}

function moneyBaseOnKm(numberOfKm, waitingTime, arrayPrice, waitingPrice) {
    var waitPay = priceOfWait(waitingTime, waitingPrice);
    if (numberOfKm <= 1) {
        return waitPay + arrayPrice[0];
    } else if (numberOfKm > 1 && numberOfKm <= 20) {
        return waitPay + arrayPrice[0] + (numberOfKm - 1) * arrayPrice[1];
    } else if (numberOfKm > 20) {
        return waitPay + arrayPrice[0] + 19 * arrayPrice[1] + (numberOfKm - 20) * arrayPrice[2];
    }

}

//no need to insert paramater into function sumOfMoney coz taking them directly from HTML
function sumMoney() {
    var numberOfKm = document.getElementById("numberOfKm").value;
    var waitingTime = document.getElementById("waitingTime").value;

    numberOfKm = parseFloat(numberOfKm);
    waitingTime = parseFloat(waitingTime);

    var sumOfMoney = 0;
    var carType = checkCarType();
    switch (carType) {
        case "uberX":
            sumOfMoney = moneyBaseOnKm(numberOfKm, waitingTime, ARRAY_UBER_X_PRICE, UBER_X_WAIT_PRICE);
            break;
        case "uberSUV":
            sumOfMoney = moneyBaseOnKm(numberOfKm, waitingTime, ARRAY_UBER_SUV_PRICE, UBER_SUV_WAIT_PRICE);
            break;
        case "uberBlack":
            sumOfMoney = moneyBaseOnKm(numberOfKm, waitingTime, ARRAY_UBER_BLACK_PRICE, UBER_BLACK_WAIT_PRICE);
            break;
        default:
            alert("Please choose car type!");
    }
    return sumOfMoney;
    }


//addEvent_onclick
document.getElementById("btnPayMoney").onclick = function () {
    var money = sumMoney();
    document.getElementById("divThanhTien").style.display = "block";
    document.getElementById("printMoney").innerHTML = money;
}

//create table of price, cartype,and arrayKm
function renderRowOfKm(carType, arrayKm, arrayPrice, tblBody){
    for (var i = 0; i < arrayKm.length; i++) {
        var tr = document.createElement("tr");

        var tdCarType = document.createElement("td");
        var tdUseKm = document.createElement("td");
        var tdPriceOfEachType = document.createElement("td");
        var tdPayMent = document.createElement("td");

        tdCarType.innerHTML = carType;
        tdUseKm.innerHTML = arrayKm[i] + " km";
        tdPriceOfEachType.innerHTML = arrayPrice[i];
        tdPayMent.innerHTML = arrayKm[i] * arrayPrice[i];

        tr.appendChild(tdCarType);
        tr.appendChild(tdUseKm);
        tr.appendChild(tdPriceOfEachType);
        tr.appendChild(tdPayMent);

        tblBody.appendChild(tr);
    }
}

    function renderRowOfWaitingTime(waitingTime, waitingPrice, tblBody) {
        var waitingPayment = priceOfWait(waitingTime, waitingPrice);
        var trWaitingTime = document.createElement("tr");

        var tdMinuteTitle = document.createElement("td");
        var tdMinutes = document.createElement("td");
        var tdUnitPrice = document.createElement("td");
        var tdPayment = document.createElement("td");

        tdMinuteTitle.innerHTML = "Waiting time (minutes)";
        tdMinutes.innerHTML = waitingTime + "minutes";
        tdUnitPrice.innerHTML = waitingPrice;
        tdPayment.innerHTML = waitingPayment;

        trWaitingTime.appendChild("tdMinuteTitle");
        trWaitingTime.appendChild("tdMinutes");
        trWaitingTime.appendChild("tdUnitPrice");
        trWaitingTime.appendChild("tdPayment");

        tblBody.appendChild(trWaitingTime);
    }

    function renderRowTotal(sumOfMoney, tblBody){
        var trTotal = document.createElement("tr");
        trTotal.className = "alert alert-success";

        var tdTotalTitle =document.createElement("td");
        tdTotalTitle.setAttribute("colspan", 3);
        var tdTotal = document.createElement("td");

        tdTotalTitle.innerHTML = " Total amount of money needs to pay:"
        tdTotal.innerHTML= sumOfMoney;

        trTotal.appendChild(tdTotalTitle);
        trTotal.appendChild(tdTotal);

        tblBody.appendChild(trTotal);
    }

    function printInvoice (carType, numberOfKm, waitingTime, waitingPrice, arrayPrice, sumOfMoney){
        var tblBody= document.getElementById("tblBody");
        tblBody.innerHTML = ""; //reset body again
        
        if(numberOfKm<=1){
            renderRowOfKm(carType, [1], arrayPrice, tblBody);
        }
         else if( numberOfKm >1 && numberOfKm <=20){
            renderRowOfKm(carType, [1, numberOfKm-1], arrayPrice, tblBody);
        }
        else if( numberOfKm > 20){
            renderRowOfKm(carType, [1, 19, numberOfKm -20], arrayPrice, tblBody);
        }
         //waiting time
    if( waitingTime > 2){
        renderRowOfWaitingTime(waitingTime, waitingPrice, tblBody);
    }
    //sumOfMoney
     renderRowTotal(sumOfMoney, tblBody);

    }
  /** document.getElementById("btnPrintInvoice").onclick=function(){
       var numberOfKm = document.getElementById("numberOfKm").value;
       var waitingTime = document.getElementById("waitingTime").value;
   }
    */
   document.getElementById("btnPrintInvoice").onclick= function(){
       var result = getData();
       var sumOfMoney = sumMoney();
       var carType = checkCarType();
       switch (carType){
           case"uberX":
           printInvoice(carType, result[0], result[1], UBER_X_WAIT_PRICE, ARRAY_UBER_X_PRICE, sumOfMoney);
           break;
           case"uberSUV":
           printInvoice(carType, result[0], result[1], UBER_SUV_WAIT_PRICE, ARRAY_UBER_SUV_PRICE, sumOfMoney);
           break;
           case"uberBlack":
           printInvoice(carType, result[0], result[1], UBER_BLACK_WAIT_PRICE, ARRAY_UBER_BLACK_PRICE, sumOfMoney);
           break;
           default:
               alert("Please choose car type");
       }
   }


   function getData (){
       var result =[];
       var numberOfKm = document.getElementById("numberOfKm").value;
       numberOfKm=parseFloat(numberOfKm);
       result.push(numberOfKm);
       var waitingTime = document.getElementById("waitingTime").value;
       waitingTime = parseFloat(waitingTime);
       result.push(waitingTime);
       return result;
   }






